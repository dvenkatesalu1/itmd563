﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MortgageCalculator;
using MortgageCalculator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MortgageCalculator.Tests
{
    [TestClass()]
    public class MPCHelperTests
    {
        [TestMethod()]
        public void GetMonthlyPaymentResultTest()
        {
            double monthlyPayment = 0.0;
            //String expected = "The monthly payment is ₹ 93.22";
            String expected = "The monthly payment is $ 93.22";
            //Test correct input
            String res = MPCHelper.GetMonthlyPaymentResult("5000", "5", "4.5", ref monthlyPayment);
            Assert.AreEqual(expected, res);
            Assert.AreEqual(Math.Round(monthlyPayment, 2), 93.22);
        }

        [TestMethod()]
        public void InvalidFormInputTest()
        {
            String res = String.Empty;
            String expectedError = "Please enter a valid Principle Amount.";
            expectedError += "<br />Please enter a valid loan duration.";

            //Test Invalid Inputs
            try
            {
                double monthlyPayment = 0.0;
                res = MPCHelper.GetMonthlyPaymentResult("sdfg", "dfg", "4.5", ref monthlyPayment);
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }

            Assert.AreEqual(expectedError, res);
        }

        [TestMethod()]
        public void InvalidPrincipleAmountTest()
        {
            String res = String.Empty;
            String expectedError = "Please enter a valid Principle Amount.";

            //Test Invalid Principle Amount
            try
            {
                double monthlyPayment = 0.0;
                res = MPCHelper.GetMonthlyPaymentResult("sdfg", "5", "4.5", ref monthlyPayment);
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }

            Assert.AreEqual(expectedError, res);
        }

        [TestMethod()]
        public void InvalidTenureTest()
        {
            String res = String.Empty;
            String expectedError = "Please enter a valid loan duration.";

            //Test Invalid Tenure
            try
            {
                double monthlyPayment = 0.0;
                res = MPCHelper.GetMonthlyPaymentResult("5000", "dfg", "4.5", ref monthlyPayment);
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }

            Assert.AreEqual(expectedError, res);
        }

        [TestMethod()]
        public void GetMonthlyPaymentResultTestWithClassObject()
        {
            String expected = "With a principal of $5000, duration of 5 years and a interest rate of 4.5%, the monthly loan payment amount is $93.22";
            //Test correct input
            MortgageInfo mortgageInfo = new MortgageInfo();
            mortgageInfo.PrincipleAmount = 5000;
            mortgageInfo.Tenure = 5;
            mortgageInfo.InterestRate = 4.5;
            MPCHelper.GetMonthlyPaymentResult( ref mortgageInfo);
            Assert.AreEqual(expected, mortgageInfo.MonthlyPaymentDescription);
            Assert.AreEqual(mortgageInfo.MonthlyPayment, 93.22);
        }
    }
}