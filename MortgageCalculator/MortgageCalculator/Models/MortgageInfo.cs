﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MortgageCalculator.Models
{
    public class MortgageInfo
    {
        [DisplayName("Principal Amount")]
        [Required]
        public double PrincipleAmount { get; set; }
        [DisplayName("Loan Duration")]
        [Required]
        public int Tenure { get; set; }
        [DisplayName("Rate Of Interest")]
        [Required]
        public double InterestRate { get; set; }
        public double MonthlyPayment { get; set; }
        [DisplayName("Monthly Payment")]
        public string MonthlyPaymentDescription { get; set; }
    }
}