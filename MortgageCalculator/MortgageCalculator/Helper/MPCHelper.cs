﻿using MortgageCalculator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MortgageCalculator
{
    public class MPCHelper
    {
        private static Tuple<bool, double> ValidateInputAmount(string input)
        {
            double res;

            if (double.TryParse(input, out res))
            {
                return new Tuple<bool, double>(true, res);
            }
            else
            {
                return new Tuple<bool, double>(false, res);
            }
        }

        private static Tuple<bool, int> ValidateInputTenure(string input)
        {
            int res;

            if (int.TryParse(input, out res))
            {
                return new Tuple<bool, int>(true, res);
            }
            else
            {
                return new Tuple<bool, int>(false, res);
            }
        }

        private static double ComputeMonthlyPayment(double principal, double years, double rate)
        {
            double monthly = 0;
            double top = principal * rate / 1200.00;
            double bottom = 1 - Math.Pow(1.0 + rate / 1200.0, -12.0 * years);
            // http://www.bankrate.com/calculators/mortgages/loan-calculator.aspx
            monthly = top / bottom;
            //Console.WriteLine();
            //Console.WriteLine("With a principl of ${0}, duration of {1} years and a interest rate of {2}% the monthly loan payment amount is {3:$0.00}", principal, years, rate, monthly);
            return monthly;
        }

        public static string GetMonthlyPaymentResult(string principle, string tenure, string rate, ref double monthlyPayment)
        {
            Tuple<bool, double> pplAmt = ValidateInputAmount(principle);
            Tuple<bool, int> n = ValidateInputTenure(tenure);
            Tuple<bool, double> r = ValidateInputAmount(rate);
            String result = String.Empty;

            if( pplAmt.Item1 && n.Item1 && r.Item1 )
            {
                monthlyPayment = ComputeMonthlyPayment(pplAmt.Item2, n.Item2, r.Item2);
                result = $"The monthly payment is {monthlyPayment:c2}";
            }
            else
            {
                if (!pplAmt.Item1)
                    result = "Please enter a valid Principle Amount.";
                if(!n.Item1)
                {
                    if (result.Length > 0)
                        result += "<br />Please enter a valid loan duration.";
                    else
                        result = "Please enter a valid loan duration.";
                }
                
                if(!r.Item1)
                {
                    if (result.Length > 0)
                        result += "<br />Please enter a valid interest rate.";
                    else
                        result = "Please enter a valid interest rate.";
                }
                throw (new Exception(result));
            }

            return result;
        }

        public static void GetMonthlyPaymentResult(ref MortgageInfo mi)
        {

            mi.MonthlyPayment = Math.Round(ComputeMonthlyPayment(mi.PrincipleAmount, mi.Tenure, mi.InterestRate), 2);
            mi.MonthlyPaymentDescription = $"With a principal of ${mi.PrincipleAmount}, duration of {mi.Tenure} years and a interest rate of {mi.InterestRate}%, the monthly loan payment amount is {mi.MonthlyPayment:$0.00}";

        }
    }
}