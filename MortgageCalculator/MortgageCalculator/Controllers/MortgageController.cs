﻿using MortgageCalculator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MortgageCalculator.Controllers
{
    public class MortgageController : Controller
    {
        // GET: Mortgage
        public ActionResult Index()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Index([Bind(Include = "PrincipleAmount, Tenure, InterestRate")] MortgageInfo mortgageInfo)
        {
            if(ModelState.IsValid)
            {
                MPCHelper.GetMonthlyPaymentResult(ref mortgageInfo);
                return View("MonthlyPayment", mortgageInfo);
            }
            else
            {
                return View("Index", mortgageInfo);
            }
            
        }
    }


}